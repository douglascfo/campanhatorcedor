package br.com.campanhasociotorcedor.api;


import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.campanhasociotorcedor.domain.orm.Campanha;
import br.com.campanhasociotorcedor.domain.orm.Cliente;
import br.com.campanhasociotorcedor.service.ClienteService;

//API de serviço de cliente,usada para CRUD e outros serviços de cliente
@RestController
@AllArgsConstructor
@RequestMapping("cliente")
public class ClienteRestService {

	@Autowired
    private ClienteService clienteService;
		
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Cliente getById(@PathVariable Integer id) {
    	return clienteService.getById(id);
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/listaClientes", method = RequestMethod.GET)
    public Iterable<Cliente> getAllClientes() {
    	return clienteService.findAll();
    }
              
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value="/salvarCliente", method = RequestMethod.POST)
    public Cliente save(@RequestBody Cliente cliente) {
        return clienteService.saveOrUpdate(cliente);
    }
    
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value="/vincularClienteCampanha", method = RequestMethod.POST)
    public Cliente vincularClienteCampanha(@RequestBody Cliente cliente,@RequestBody List<Campanha> campanhas) {    
    	cliente.setCampanhas(campanhas);
        return clienteService.saveOrUpdate(cliente);
    }
    
    
    
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value="/alterarCliente", method = RequestMethod.POST)//Verificar
    public Cliente update(@RequestBody Cliente cliente) {
        return clienteService.saveOrUpdate(cliente);
    }
        
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/excluirCliente/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable Integer id) {
    	return clienteService.delete(id);
    }
    
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/carregamentoClientePadrao", method = RequestMethod.GET)
    public String carregaCampanhas() {  
    	String resposta = "";
    	
    	Cliente cliente = new Cliente();
    	cliente.setIdTimeCoracao(1);
    	cliente.setNomeCompleto("José Pereira");
    	cliente.setDataNascimento(null);
    	cliente.setEmail("jp@t.com");
    	
    	Campanha campanha = new Campanha();
    	Campanha campanha1 = new Campanha();
    	List lista = new ArrayList<Campanha>();
    	campanha.setIdCampanha(10);
    	lista.add(campanha);
    	campanha1.setIdCampanha(11);
    	lista.add(campanha1);
    	cliente.setCampanhas(lista);
    	    	
    	Cliente cliente1 = new Cliente();
    	cliente1.setIdTimeCoracao(1);
    	cliente1.setNomeCompleto("Maria Luzia");
    	cliente1.setDataNascimento(null);
    	cliente1.setEmail("mluzia@t.com");
    	
    	if(clienteService.getByEmail(cliente.getEmail()) == null){
    		clienteService.saveOrUpdate(cliente);
    		resposta = "Carregamento Padrão Realizado";
    	}else{
    		resposta = "Cadastro com e-mail já existente. \n Caso queira vincular uma campanha a este usuario, acesse: <a target=\"_blank\" href=http://localhost:8080/cliente/vincularClienteCampanha>Vincular Cliente Campanha</a>";
    	}
    	
    	
    	clienteService.saveOrUpdate(cliente1);
    	
    	return resposta;
    }
    
    
    
}
