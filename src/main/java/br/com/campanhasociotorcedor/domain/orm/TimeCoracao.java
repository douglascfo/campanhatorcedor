package br.com.campanhasociotorcedor.domain.orm;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

//Entidade TimeCoração
@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TimeCoracao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer idTime;		
	private String nomeTime;
	
	public Integer getIdTime() {
		return idTime;
	}
	public void setIdTime(Integer idTime) {
		this.idTime = idTime;
	}
	public String getNomeTime() {
		return nomeTime;
	}
	public void setNomeTime(String nomeTime) {
		this.nomeTime = nomeTime;
	}	
	
		
	
}
