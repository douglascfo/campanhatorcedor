package br.com.campanhasociotorcedor.domain.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.campanhasociotorcedor.domain.orm.TimeCoracao;

//Interface usada para personalizar queries da camada DAO da entidade TimeCoracao
@Repository
public interface TimeRepository extends CrudRepository<TimeCoracao, Integer> {}
