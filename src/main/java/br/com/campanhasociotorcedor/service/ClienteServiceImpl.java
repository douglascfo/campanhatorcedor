package br.com.campanhasociotorcedor.service;


import static java.util.Objects.isNull;
import lombok.AllArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.campanhasociotorcedor.domain.orm.Cliente;
import br.com.campanhasociotorcedor.domain.repository.ClienteRepository;

//Classe responsavel pela ligação com a camada de persistencia de Cliente
@Service
@AllArgsConstructor
public class ClienteServiceImpl implements ClienteService {

	@Autowired
    private ClienteRepository clienteRepository;
	
    @Override
    public Iterable<Cliente> findAll() {
        return clienteRepository.findAll();
    }
	
    @Override
    public Cliente getById(Integer id) {
    	Cliente cliente = clienteRepository.findOne(id);
        if(isNull(cliente)) {
            throw new RuntimeException("Cliente não encontrada!");
        }
        return cliente;
    }

    @Override
    public Cliente saveOrUpdate(Cliente cliente) {
        return clienteRepository.save(cliente);
    }
    
    
    @Override
    public String delete(Integer id) {
    	try{
    		clienteRepository.delete(id);
    	}catch(Exception ex){
    		return "Problema Deleção Cliente";	
    	}    	
        return "Cliente Removida";
    }

	@Override
	public Cliente getByEmail(String email) {
		return clienteRepository.findByEmail(email);
	}
    
    
}
