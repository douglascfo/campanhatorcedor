package br.com.campanhasociotorcedor.service;


import br.com.campanhasociotorcedor.domain.orm.TimeCoracao;

//Interface TimeCoracao
public interface TimeService {

	TimeCoracao getById(Integer id);
	String delete(Integer id);
	TimeCoracao saveOrUpdate(TimeCoracao cliente);
	Iterable<TimeCoracao> findAll();
}
