package br.com.campanhasociotorcedor;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.campanhasociotorcedor.domain.orm.Campanha;
import br.com.campanhasociotorcedor.domain.orm.Cliente;
import br.com.campanhasociotorcedor.domain.orm.TimeCoracao;
import br.com.campanhasociotorcedor.service.CampanhaService;

public class CreationMockTest {
	
	@Mock
	private CampanhaService campanhaService;
	@Mock
	private Campanha campanha;
	@Mock
	private Cliente cliente;
	@Mock
	private TimeCoracao timeCoracao;
	
	@Before
	public void setupMock(){
		 MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testMockCreation(){
		assertNotNull(campanha);
		assertNotNull(cliente);
        assertNotNull(timeCoracao);
	}
	
}
