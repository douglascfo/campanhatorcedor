package br.com.campanhasociotorcedor.api;

import static org.junit.Assert.assertNotNull;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import br.com.campanhasociotorcedor.domain.orm.Campanha;
import br.com.campanhasociotorcedor.service.CampanhaService;

public class CampanhaRestServiceTest{

	@Mock
	private CampanhaService campanhaService;
	@Mock
	private Campanha campanha;	
	@Mock
	private Iterable<Campanha> listaCampanhas;
	
	private	SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
	private String dataAtual = sdf.format(new Date());	
	
	
	@Before
	public void setupMock(){
		 MockitoAnnotations.initMocks(this);
	}	
	
	@Test
	public void testGetById() {
		Mockito.when(campanhaService.getById(10)).thenReturn(campanha);
		assertNotNull(campanha);
	}

	@Test
	public void testGetAllCampanhasAtivas() {
		Mockito.when(campanhaService.findAllCurrent(dataAtual)).thenReturn(listaCampanhas);
		assertNotNull(listaCampanhas);	
	}

	@Test
	public void testCarregaCampanhas() {
		Mockito.when(campanhaService.saveOrUpdate(campanha)).thenReturn(campanha);
		assertNotNull(campanha);	
	}
	
}
