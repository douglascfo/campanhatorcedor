package br.com.campanhasociotorcedor.api;

import static org.junit.Assert.assertNotNull;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import br.com.campanhasociotorcedor.domain.orm.Cliente;
import br.com.campanhasociotorcedor.service.ClienteService;


public class ClienteRestServiceTest{

	@Mock
	private ClienteService clienteService;
	@Mock
	private Cliente cliente;	
	@Mock
	private Iterable<Cliente> listaClientes;
	
	
	@Before
	public void setupMock(){
		 MockitoAnnotations.initMocks(this);
	}	
	
	@Test
	public void testGetById() {
		Mockito.when(clienteService.getById(10)).thenReturn(cliente);
		assertNotNull(cliente);
	}

	@Test
	public void testCarregaClientes() {
		Mockito.when(clienteService.saveOrUpdate(cliente)).thenReturn(cliente);
		assertNotNull(cliente);	
	}
	
}
